<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AirtimeBonus extends Model
{
    public function profiles(){
        return $this->belongsTo('App\Profile');
    }
}
