<?php

namespace App\Http\Controllers;

use App\AirtimeBonus;
use Illuminate\Http\Request;

class AirtimeBonusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AirtimeBonus  $airtimeBonus
     * @return \Illuminate\Http\Response
     */
    public function show(AirtimeBonus $airtimeBonus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AirtimeBonus  $airtimeBonus
     * @return \Illuminate\Http\Response
     */
    public function edit(AirtimeBonus $airtimeBonus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AirtimeBonus  $airtimeBonus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AirtimeBonus $airtimeBonus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AirtimeBonus  $airtimeBonus
     * @return \Illuminate\Http\Response
     */
    public function destroy(AirtimeBonus $airtimeBonus)
    {
        //
    }
}
