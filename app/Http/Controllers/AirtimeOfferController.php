<?php

namespace App\Http\Controllers;

use App\AirtimeOffer;
use Illuminate\Http\Request;

class AirtimeOfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AirtimeOffer  $airtimeOffer
     * @return \Illuminate\Http\Response
     */
    public function show(AirtimeOffer $airtimeOffer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AirtimeOffer  $airtimeOffer
     * @return \Illuminate\Http\Response
     */
    public function edit(AirtimeOffer $airtimeOffer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AirtimeOffer  $airtimeOffer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AirtimeOffer $airtimeOffer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AirtimeOffer  $airtimeOffer
     * @return \Illuminate\Http\Response
     */
    public function destroy(AirtimeOffer $airtimeOffer)
    {
        //
    }
}
