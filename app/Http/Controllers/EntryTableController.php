<?php

namespace App\Http\Controllers;

use App\EntryTable;
use Illuminate\Http\Request;

class EntryTableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EntryTable  $entryTable
     * @return \Illuminate\Http\Response
     */
    public function show(EntryTable $entryTable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EntryTable  $entryTable
     * @return \Illuminate\Http\Response
     */
    public function edit(EntryTable $entryTable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EntryTable  $entryTable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EntryTable $entryTable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EntryTable  $entryTable
     * @return \Illuminate\Http\Response
     */
    public function destroy(EntryTable $entryTable)
    {
        //
    }
}
