<?php

namespace App\Http\Controllers;

use App\ProfilePoint;
use Illuminate\Http\Request;

class ProfilePointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProfilePoint  $profilePoint
     * @return \Illuminate\Http\Response
     */
    public function show(ProfilePoint $profilePoint)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProfilePoint  $profilePoint
     * @return \Illuminate\Http\Response
     */
    public function edit(ProfilePoint $profilePoint)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProfilePoint  $profilePoint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProfilePoint $profilePoint)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProfilePoint  $profilePoint
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProfilePoint $profilePoint)
    {
        //
    }
}
