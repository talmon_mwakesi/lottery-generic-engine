<?php

namespace App\Http\Controllers;

use App\RequestDeposit;
use Illuminate\Http\Request;

class RequestDepositController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RequestDeposit  $requestDeposit
     * @return \Illuminate\Http\Response
     */
    public function show(RequestDeposit $requestDeposit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RequestDeposit  $requestDeposit
     * @return \Illuminate\Http\Response
     */
    public function edit(RequestDeposit $requestDeposit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RequestDeposit  $requestDeposit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequestDeposit $requestDeposit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RequestDeposit  $requestDeposit
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequestDeposit $requestDeposit)
    {
        //
    }
}
