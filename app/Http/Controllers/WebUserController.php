<?php

namespace App\Http\Controllers;

use App\WebUser;
use Illuminate\Http\Request;

class WebUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WebUser  $webUser
     * @return \Illuminate\Http\Response
     */
    public function show(WebUser $webUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WebUser  $webUser
     * @return \Illuminate\Http\Response
     */
    public function edit(WebUser $webUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WebUser  $webUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebUser $webUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WebUser  $webUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebUser $webUser)
    {
        //
    }
}
