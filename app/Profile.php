<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public function airtime_bonus(){
        return $this->hasMany('App\AirtimeBonus');
    }
    public function user(){
        return $this->hasOne('App\User');
    }
    public function entry_tables(){
        return $this->hasMany('App\EntryTable');
    }
    public function referrals(){
        return $this->hasMany('App\Referral');
    }
    public function web_users(){
        return $this->hasMany('App\WebUser');
    }
    public function request_deposits(){
        return $this->hasMany('App\RequestDeposit');
    }
}
