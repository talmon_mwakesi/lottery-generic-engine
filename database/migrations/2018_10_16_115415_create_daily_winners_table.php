<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyWinnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_winners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('profile_id');
            $table->integer('total_points');
            $table->integer('amount_won');
            $table->date('date_won');
            $table->string('created_by');
            $table->timestamp('modified')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->unique(['profile_id_date_won','profile_id','date_won']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_winners');
    }
}
