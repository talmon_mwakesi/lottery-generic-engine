<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit', function (Blueprint $table) {
            $table->increments('id');
            $table->string('msisdn');
            $table->bigInteger('profile_id');
            $table->string('deposit_code');
            $table->decimal('amount', 10, 2);
            $table->integer('status')->default(1);
            $table->string('reference_id');
            $table->string('paybill_no');
            $table->string('pg_trx_id');
            $table->string('KYC');
            $table->string('created_by');
            $table->timestamp('modified')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->unique(['deposit_code_msisdn','deposit_code','msisdn']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit');
    }
}
