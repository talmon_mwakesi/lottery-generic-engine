<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('msisdn');
            $table->string('choice');
            $table->integer('stake');
            $table->integer('inbox_id');
            $table->string('source');
            $table->integer('draw_id');
            $table->enum('play_status', ['AWAITING RESPONSE', 'RESPONDED'])->default('AWAITING RESPONSE');
            $table->enum('play_response', ['WAITING FOR RESPONSE', 'AIRTIME','PRIZE','TIMEOUT'])->default('WAITING FOR RESPONSE');
            $table->tinyInteger('airtime_offered');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entries');
    }
}
