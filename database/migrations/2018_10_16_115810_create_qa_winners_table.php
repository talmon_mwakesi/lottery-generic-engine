<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQaWinnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qa_winners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id');
            $table->integer('profile_point_id');
            $table->string('draw_time');
            $table->integer('winning_points');
            $table->double('prize');
            $table->string('varchar');
            $table->tinyInteger('is_airtime')->default(0);
            $table->timestamp('modified')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qa_winners');
    }
}
