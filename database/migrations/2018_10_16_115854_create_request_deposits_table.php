<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_deposits', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('profile_id');
            $table->bigInteger('msisdn');
            $table->integer('amount');
            $table->integer('status');
            $table->string('mpesa_code');
            $table->timestamp('modified')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_deposits');
    }
}
