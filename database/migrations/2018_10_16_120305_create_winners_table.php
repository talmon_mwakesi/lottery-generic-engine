<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWinnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('winners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('qa_winner_id');
            $table->bigInteger('profile_id');
            $table->string('msisdn');
            $table->integer('total_questions_answered');
            $table->string('status')->default('NOT PAID');
            $table->tinyInteger('is_airtime')->default(0);
            $table->decimal('amount', 10, 2);
            $table->string('created_by');
            $table->string('updated_by');
            $table->timestamp('modified')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('winners');
    }
}
