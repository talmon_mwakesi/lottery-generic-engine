<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdrawals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inbox_id');
            $table->string('msisdn');
            $table->string('raw_text');
            $table->decimal('amount', 64, 2);
            $table->string('reference');
            $table->string('created_by');
            $table->enum('status', ['PROCESSING','QUEUED','SUCCESS','FAILED','TRX_SUCCESS','CANCELLED','SENT','UNKNOWN','REVERSED']);
            $table->string('provider_reference');
            $table->integer('number_of_sends')->default(0);
            $table->float('charge')->default(30);
            $table->unique(['provider_reference','inbox_id','profile_id','reference','created','reference','status']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdrawals');
    }
}
